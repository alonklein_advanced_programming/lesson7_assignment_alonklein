﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.Windows.Forms;
using System.Threading;
using System.Net.Sockets;
using System.Net;

namespace hw_20
{
    public partial class Form1 : Form
    {
        private Dictionary<string, Bitmap> cards;
        public delegate void method();
        private string write = null;
        private bool startGame;
        private bool endGame = false;
        private bool choose = false;
        
        public Form1()
        { 
           
            InitializeComponent();
            handleCardsDic();
            GameInit();
            
        }
        private void handleCardsDic()
        {
          
            cards = new Dictionary<string, Bitmap>();
            cards.Add("01C", hw_20.Properties.Resources.ace_of_clubs);
            cards.Add("01D", hw_20.Properties.Resources.ace_of_diamonds);
            cards.Add("01H", hw_20.Properties.Resources.ace_of_hearts);
            cards.Add("01S", hw_20.Properties.Resources.ace_of_spades2);


            cards.Add("13C", hw_20.Properties.Resources.king_of_clubs2);
            cards.Add("13D", hw_20.Properties.Resources.king_of_diamonds2);
            cards.Add("13H", hw_20.Properties.Resources.king_of_hearts2);
            cards.Add("13S", hw_20.Properties.Resources.king_of_spades2);


            cards.Add("12C", hw_20.Properties.Resources.queen_of_clubs2);
            cards.Add("12D", hw_20.Properties.Resources.queen_of_diamonds2);
            cards.Add("12H", hw_20.Properties.Resources.queen_of_hearts2);
            cards.Add("12S", hw_20.Properties.Resources.queen_of_spades2);

            cards.Add("11C", hw_20.Properties.Resources.jack_of_clubs2);
            cards.Add("11D", hw_20.Properties.Resources.jack_of_diamonds2);
            cards.Add("11H", hw_20.Properties.Resources.jack_of_hearts2);
            cards.Add("11S", hw_20.Properties.Resources.jack_of_spades2);

            cards.Add("02C", hw_20.Properties.Resources._2_of_clubs);
            cards.Add("03C", hw_20.Properties.Resources._3_of_clubs);
            cards.Add("04C", hw_20.Properties.Resources._4_of_clubs);
            cards.Add("05C", hw_20.Properties.Resources._5_of_clubs);
            cards.Add("06C", hw_20.Properties.Resources._6_of_clubs);
            cards.Add("07C", hw_20.Properties.Resources._7_of_clubs);
            cards.Add("08C", hw_20.Properties.Resources._8_of_clubs);
            cards.Add("09C", hw_20.Properties.Resources._9_of_clubs);
            cards.Add("10C", hw_20.Properties.Resources._10_of_clubs);
            
            cards.Add("02D", hw_20.Properties.Resources._2_of_diamonds);
            cards.Add("03D", hw_20.Properties.Resources._3_of_diamonds);
            cards.Add("04D", hw_20.Properties.Resources._4_of_diamonds);
            cards.Add("05D", hw_20.Properties.Resources._5_of_diamonds);
            cards.Add("06D", hw_20.Properties.Resources._6_of_diamonds);
            cards.Add("07D", hw_20.Properties.Resources._7_of_diamonds);
            cards.Add("08D", hw_20.Properties.Resources._8_of_diamonds);
            cards.Add("09D", hw_20.Properties.Resources._9_of_diamonds);
            cards.Add("10D", hw_20.Properties.Resources._10_of_diamonds);
                         
            cards.Add("02H", hw_20.Properties.Resources._2_of_hearts);
            cards.Add("03H", hw_20.Properties.Resources._3_of_hearts);
            cards.Add("04H", hw_20.Properties.Resources._4_of_hearts);
            cards.Add("05H", hw_20.Properties.Resources._5_of_hearts);
            cards.Add("06H", hw_20.Properties.Resources._6_of_hearts);
            cards.Add("07H", hw_20.Properties.Resources._7_of_hearts);
            cards.Add("08H", hw_20.Properties.Resources._8_of_hearts);
            cards.Add("09H", hw_20.Properties.Resources._9_of_hearts);
            cards.Add("10H", hw_20.Properties.Resources._10_of_hearts);

            cards.Add("02S", hw_20.Properties.Resources._2_of_spades);
            cards.Add("03S", hw_20.Properties.Resources._3_of_spades);
            cards.Add("04S", hw_20.Properties.Resources._4_of_spades);
            cards.Add("05S", hw_20.Properties.Resources._5_of_spades);
            cards.Add("06S", hw_20.Properties.Resources._6_of_spades);
            cards.Add("07S", hw_20.Properties.Resources._7_of_spades);
            cards.Add("08S", hw_20.Properties.Resources._8_of_spades);
            cards.Add("09S", hw_20.Properties.Resources._9_of_spades);
            cards.Add("10S", hw_20.Properties.Resources._10_of_spades);

            
        }
        private void label1_Click(object sender, EventArgs e)
        {

        }
        private void GameInit()
        {
            startGame = false;
            setupConnection();
           
        }
        private void unFreeze()
        {
            ExitBtm.Enabled = true;
            startGame = true;
            Generate();
        }


        private NetworkStream clientStream;
        private void setupConnection()
        {
            TcpClient client = new TcpClient();
            IPEndPoint serverEndPoint = new
            IPEndPoint(IPAddress.Parse("127.0.0.1"), 8820);
            client.Connect(serverEndPoint);
            clientStream = client.GetStream();
            Thread reader = new Thread(StreamReader);
            reader.Start();

            Thread writer = new Thread(StreamWriter);
            writer.Start();
           
        }

        private void StreamReader(object obj)
        { bool testing = false;
            while(!endGame)
            {
                byte[] buffer = new byte[4096];
                int bytesRead = clientStream.Read(buffer, 0, 1);
                if (!startGame)
                {
                    method a = unFreeze;
                    Invoke(a);
                }
                Thread.Sleep(300);

            }

            
        }

        private void processData(string data)
        {
            char code = data.ElementAt(0);
            if (code == '1') ;
                

                
        }

        private void StreamWriter(object obj)
        {
            bool testing = false;
            while (!endGame)
            {
                if(write!= null)
                {
                    clientStream.Write(Encoding.ASCII.GetBytes(write), 0, write.Length);
                    write = null;
                }
                    

                Thread.Sleep(300);
            }
        }

        private string randomCard()
        {
            string[] cardsNames = new string[]{"C","D","H","S"};

            Random rnd = new Random();
            string cardNum = rnd.Next(1, 13).ToString().PadLeft(2, '0') + cardsNames[rnd.Next(0, 4)];
            Console.WriteLine(cardNum);
            return cardNum;
        }

        private Bitmap getCard(string card)
        {
            Bitmap ret = null;
            cards.TryGetValue(card,out ret);
            return ret;
        }
        private void Generate()
        {
            Bitmap card = hw_20.Properties.Resources.card_back_red;
            // set the location for where we start drawing the new cards (notice the location+height)
            Point nextLocation = new Point(pic1.Location.X, pic1.Location.Y+ pic1.Size.Height + 10);
            int currentIndex = 0;
            for (int i = 0; i < 10; i++)
            {
                
                // create the image object itself
                System.Windows.Forms.PictureBox currentPic = new PictureBox();
                currentPic.Name = "picDynamic" + i; // in our case, this is the only property that changes between the different images
                currentPic.Image = global::hw_20.Properties.Resources.card_back_red;
                currentPic.Location = nextLocation;
                currentPic.Size = new System.Drawing.Size(100, 114);
                currentPic.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;

                // assign an event to it
                currentPic.Click += delegate (object sender1, EventArgs e1)
                {
                    choose = true;
                    currentPic.Name.Substring(currentPic.Name.Length - 1);
                  
                    //MessageBox.Show("You've clicked card #" + currentIndex);
                    string randCard = randomCard();
                    ((PictureBox)sender1).Image = getCard(randCard);
                    string code = "1" + randCard;
                    write = code;
                    // use the delegate's params in order to remove the specific image which was clicked
                    //((PictureBox)sender1).Hide();
                    // ((PictureBox)sender1).Dispose();
                };

                // add the picture object to the form (otherwise it won't be seen)
                this.Controls.Add(currentPic);

                // calculate the location point for the next card to be drawn/added
                nextLocation.X += currentPic.Size.Width + 10;
                if (nextLocation.X > this.Size.Width)
                {
                    // move to next line below
                    nextLocation.X = pic1.Location.X;
                    nextLocation.Y += currentPic.Size.Height + 10;
                }
            }
        }

        private void Form1_Load(object sender, EventArgs e)
        {

        }

        private void ExitBtm_Click(object sender, EventArgs e)
        {
            endGame = true;
            Application.Exit();
        }
    }
}
